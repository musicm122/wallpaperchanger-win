﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;

namespace FileIO
{
    public class FileCollection:IEnumerable<FileInfo>
    {
        List<FileInfo> _walls = new List<FileInfo>();

        public FileCollection() 
        {
            _walls = new List<FileInfo>();
        }

        public IEnumerator<FileInfo> GetEnumerator()
        {
            foreach (FileInfo item in _walls)
            {
                yield return item;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator<string> Paths
        {
            get
            {
                foreach (FileInfo item in _walls)
                {
                    yield return item.FullName;
                    
                }
            }
        }

        public IEnumerator<string> Filename
        {
            get
            {
                foreach (FileInfo item in _walls)
                {
                    yield return item.Name;

                }
            }
        }

        public IEnumerator<string> FileExtention 
        {
            get
            {
                foreach (FileInfo item in _walls)
                {
                    yield return item.Extension;
                }
            }
        }
        
        public void Add(FileInfo item)
        {
            this._walls.Add(item);
        }

        public string this[int index]
        {
            get { return this[index]; }
        }
    }
    

}
