﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileIO
{
    public class DirectoryHelper
    {
        public static List<FileInfo> PromptForPictures()
        {
            List<FileInfo> selectedFileNames = new List<FileInfo>();

            string filter = "BMP|*.bmp|GIF|*.gif|JPG|*.jpg;*.jpeg|PNG|*.png";

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = filter;
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    selectedFileNames.AddRange(openFileDialog.FileNames.Select(fn => new FileInfo(fn)));
                }
            }

            return selectedFileNames;
        }

        public static FileInfo PromptForPicture()
        {
            FileInfo selectedFileNames = null;

            string filter = "BMP|*.bmp|GIF|*.gif|JPG|*.jpg;*.jpeg|PNG|*.png";

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Multiselect = false;
                openFileDialog.Filter = filter;
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    selectedFileNames = new FileInfo(openFileDialog.FileName);
                }
            }

            return selectedFileNames;
        }

        public static DirectoryInfo PromptForFolder()
        {
            DirectoryInfo folder = new DirectoryInfo("\\");
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    folder = new DirectoryInfo(fbd.SelectedPath);
                }
            }
            return folder;
        }

        public static async Task<IEnumerable<FileInfo>> GetFilesByFilter(DirectoryInfo directory, string filter) =>
            await Task.Run<IEnumerable<FileInfo>>(() => directory.GetFiles(filter, SearchOption.AllDirectories));


        public static async Task<IEnumerable<FileInfo>> GetFilesByFilter(IEnumerable<DirectoryInfo> directories, string filter) =>
            await Task.Run<IEnumerable<FileInfo>>(() => directories.SelectMany(dir => dir.GetFiles(filter, SearchOption.AllDirectories)));
    }
}