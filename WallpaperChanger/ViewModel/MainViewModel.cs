﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using WinWallpaperApi;
using System.Collections.ObjectModel;
using System.IO;
using FileIO;
using Microsoft.Win32;
using System.Windows.Forms;
using Utilities.DataTypes.ExtensionMethods;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace WallpaperChanger.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private string currentWallpaper;

        #region Constructor

        public MainViewModel()
        {
            Init();
            //this.AddDirectory = new DelegateCommand((x) => { dm.AddDirectiory(x.ToString()); }, CanAddDirectory);
            //this.AddFile = new DelegateCommand((x) => { dm.AddFile(x.ToString()); }, CanAddFile);
            this.SetWallpaper = new RelayCommand(SetCurrentWallpaper, CanSetWallpaper);
            this.BrowseForPicture = new RelayCommand(BrowseForFile);
        }

        #endregion

        void Init()
        {
            CurrentWallpaper = WinWallpaperApi.Wallpaper.GetCurrentWallpaper();
        }

        private async Task LoadData()
        {
            var filter = "*.jpg|*.png|*.bmp";
            var folder = DirectoryHelper.PromptForFolder();
            var files = await FileIO.DirectoryHelper.GetFilesByFilter(folder, filter);
            WallList.AddRange(files);
            SearchableDirectory.Add(folder);

        }

        #region DelegateCommand
        public RelayCommand SetWallpaper { get; set; }
        public RelayCommand AddDirectory { get; set; }
        public RelayCommand BrowseForPicture { get; set; }

        #endregion

        #region ObservableCollection
        public ObservableCollection<DirectoryInfo> SearchableDirectory { get; set; }

        public ObservableCollection<FileInfo> WallList { get; set; }
        #endregion

        public Wallpaper.Style OrientationStyle { get; set; }

        public string CurrentWallpaper
        {
            get { return currentWallpaper; }
            set { Set(ref currentWallpaper, value); }
        }

        public string ErrorMessage
        {
            get; set;
        }

        #region Private Members
        private void SetCurrentWallpaper()
        {
            Wallpaper.Set(new Uri(this.CurrentWallpaper), Wallpaper.Style.Stretched);
            RaisePropertyChanged("CurrentWallpaper");
        }

        private bool CanSetWallpaper() =>
            File.Exists(CurrentWallpaper);

        private void BrowseForFile()
        {
            var file = DirectoryHelper.PromptForPicture();
            this.CurrentWallpaper = file.FullName;
        }

        #endregion

    }
}
