﻿using System;
using System.Windows.Input;

namespace WinWallpaperApi
{
    /// <summary>
    /// Used to determine if a method can be executed and calls execution
    /// </summary>
    public class DelegateCommand : ICommand
    {
        //
        /// <summary>
        /// Gets or sets the method to execute
        /// </summary>
        /// <value>The execute.</value>
        public Action<object> execute { get; set; }

        /// <summary>
        /// Gets or sets the can execute method. Determine if execution should occur and the state of the binding element.
        /// </summary>
        /// <value>The can execute.</value>
        public Predicate<object> canExecute { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="DelegateCommand"/> class.
        /// </summary>
        public DelegateCommand()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DelegateCommand"/> class.
        /// </summary>
        /// <param name="executeMethod">The execute method.</param>
        public DelegateCommand(Action<object> executeMethod)
        {
            this.execute = executeMethod;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DelegateCommand"/> class.
        /// </summary>
        /// <param name="executeMethod">The execute method.</param>
        /// <param name="canExecuteMethod">The can execute method.</param>
        public DelegateCommand(Action<object> executeMethod, Predicate<object> canExecuteMethod)
        {
            this.execute = executeMethod;
            this.canExecute = canExecuteMethod;
        }

        /// <summary>
        /// Defines the method to be called when the command is invoked.
        /// </summary>
        /// <param name="parameter">Data used by the command.  If the command does not require data to be passed, this object can be set to null.</param>
        public void Execute(object parameter)
        {
            this.execute(parameter);
        }

        /// <summary>
        /// Defines the method that determines whether the command can execute in its current state.
        /// </summary>
        /// <param name="parameter">Data used by the command.  If the command does not require data to be passed, this object can be set to null.</param>
        /// <returns>
        /// true if this command can be executed; otherwise, false.
        /// </returns>
        public bool CanExecute(object parameter)
        {
            return canExecute == null ? true : canExecute(parameter);
        }

        /// <summary>
        /// Occurs when changes occur that affect whether or not the command should execute.
        /// </summary>
        public event EventHandler CanExecuteChanged;

        /// <summary>
        /// Fires the can execute changed.
        /// </summary>
        public void FireCanExecuteChanged()
        {
            var temp = CanExecuteChanged;
            if (temp != null)
                temp(this, EventArgs.Empty);
        }
    }
}
