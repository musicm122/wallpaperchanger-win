﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace WallpaperChanger
{
    public class WallpaperCollection:IEnumerable<WallpaperItem>
    {
        List<WallpaperItem> _walls = new List<WallpaperItem>();

        public WallpaperCollection() 
        {
            _walls = new List<WallpaperItem>();
        }

        public IEnumerator<WallpaperItem> GetEnumerator()
        {
            foreach (WallpaperItem item in _walls)
            {
                yield return item;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator<string> Paths
        {
            get
            {
                foreach (WallpaperItem item in _walls)
                {
                    yield return item.Path;
                    
                }
            }
        }

        public IEnumerator<string> Filename
        {
            get
            {
                foreach (WallpaperItem item in _walls)
                {
                    yield return item.Filename;

                }
            }
        }

        public IEnumerator<string> FileExtention 
        {
            get
            {
                foreach (WallpaperItem item in _walls)
                {
                    yield return item.FileExtention;
                }
            }
        }
        
        public void Add(WallpaperItem item)
        {
            this._walls.Add(item);
        }

        public string this[int index]
        {
            get { return this[index]; }
        }


        

    }
    
    public class WallpaperItem
    {
        public string Path { get; set; }
        public string Filename { get; set; }
        public string FileExtention { get; set; }
    }
}
